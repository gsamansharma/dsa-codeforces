import java.util.Scanner;

public class E_Max {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        long max=0;
        while(t-->0){
            long n=sc.nextLong();
            max=(n>max)?n:max;
        }
        System.out.println(max);
        sc.close();
    }
}
