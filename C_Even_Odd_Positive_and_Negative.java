import java.util.Scanner;

public class C_Even_Odd_Positive_and_Negative {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int even=0,odd=0,negative=0,positive=0;
        
        int t=sc.nextInt();
        while(t-->0){
            int n=sc.nextInt();
            if(n>0)positive++;
            else if(n<0)negative++;
            if(n%2==0)even++;
            else odd++;
        }
        sc.close();
        System.out.printf("Even: %d%nOdd: %d%nPositive: %d%nNegative: %d",even,odd,positive,negative);
    }
}
