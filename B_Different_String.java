import java.util.Scanner;

public class B_Different_String {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);    
    int t=sc.nextInt();
        while (t-->0) {
            int n = sc.nextInt(); // Number of books
            int[] pages = new int[n];
            
            // Reading pages array
            for (int i = 0; i < n; i++) {
                pages[i] = sc.nextInt();
            }
            
            // Calculate maximum pages Alice can read
            int totalSum = 0;
            for (int page : pages) {
                totalSum += page;
            }
            
            int currentSum = 0;
            int maxSum = 0;
            
            // Iterate over the books to find the maximum possible sum
            for (int i = 0; i < n - 1; i++) {
                currentSum += pages[i];
                maxSum = Math.max(maxSum, currentSum);
            }
            
            // Print the result for this test case
            System.out.println(maxSum);
        }
sc.close();

}
}
