import java.util.Scanner;

public class A_Guess_the_Maximum {
    static int t, n;
    static int[] a = new int[50005];
    static int[] dp = new int[50005];
    static int[] maxi = new int[50005];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();
        while (t-- > 0) {
            n = sc.nextInt();
            for (int i = 1; i <= n; i++) {
                a[i] = sc.nextInt();
            }
            for (int i = n; i >= 1; i--) {
                maxi[i] = Math.max(maxi[i + 1], a[i]);
                if (i + 2 <= n) {
                    dp[i] = Math.max(dp[i + 1], maxi[i + 2] + dp[i + 2]);
                } else {
                    dp[i] = Math.max(dp[i + 1], a[i]);
                }
            }
            sc.close();
            System.out.println(dp[1]);
        }
    }
}
