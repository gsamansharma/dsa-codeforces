import java.util.Scanner;

public class B_Even_Numbers {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int N=sc.nextInt();
        sc.close();
        int i=2;
        do{
            System.out.println((N<2)?-1:i);
            i+=2;
        }while(i<=N);
    }
}
