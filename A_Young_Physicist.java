import java.util.Scanner;

public class A_Young_Physicist {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        int sum_x=0,sum_y=0,sum_z=0;
        while (t-->0) {
                sum_x+= sc.nextInt();
                sum_y+= sc.nextInt(); 
                sum_z+= sc.nextInt();
        }
        sc.close();
        System.out.println((sum_x==0&&sum_y==0&&sum_z==0)?"YES":"NO");
    }
}

