import java.util.Scanner;

public class H_Data_Type_Guessing {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double n=sc.nextDouble();
        double k=sc.nextDouble();
        double a=sc.nextDouble();
        sc.close();
        double resultp=(double)((n*k)/a);
        long result=(long)((n*k)/a);
        if(resultp-result>0)
            System.out.println("double");
        else if(result<=2147483647&&result>=-2147483648)
            System.out.println("int");
        else
            System.out.println("long long");
    }
}
