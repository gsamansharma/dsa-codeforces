import java.util.Scanner;

public class E_Interval_Sweep {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int a,b;
        a=sc.nextInt();
        b=sc.nextInt();
        sc.close();
        System.out.println((Math.abs(a-b)<=1&&a!=0&&b!=0)?"YES":"NO");
    }
}
