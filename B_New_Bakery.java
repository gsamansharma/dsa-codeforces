import java.util.Scanner;

public class B_New_Bakery {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt(); // Number of test cases
        
        while (t-- > 0) {
            long n = scanner.nextLong();
            long a = scanner.nextLong();
            long b = scanner.nextLong();
            
            // Calculate maximum profit
            long maxProfit = Long.MIN_VALUE;
            
            for (long k = 0; k <= Math.min(n, b); k++) {
                long revenue = 0;
                // Calculate revenue for the first k buns
                for (long i = 1; i <= k; i++) {
                    revenue += (b - i + 1);
                }
                // Calculate revenue for the remaining (n - k) buns
                revenue += (n - k) * a;
                
                // Calculate profit
                long cost = n * a;
                long profit = revenue - cost;
                
                // Update maxProfit if current profit is higher
                if (profit > maxProfit) {
                    maxProfit = profit;
                }
            }
            
            // Print the maximum profit for this test case
            System.out.println(maxProfit);
        }
        
        scanner.close();
}
}
