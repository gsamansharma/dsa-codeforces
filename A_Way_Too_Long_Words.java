import java.util.Scanner;

public class A_Way_Too_Long_Words {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int noofwords=sc.nextInt();
        // sc.nextLine();
        while(noofwords>0){
            String word=sc.next();
            int n=word.length();
            if(n>10){
                word=word.charAt(0)+""+(n-2)+word.charAt(n-1);
            }
            System.out.println(word);
            noofwords--;
        }
        
        sc.close();

    }
}
