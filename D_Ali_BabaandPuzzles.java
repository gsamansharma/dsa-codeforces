import java.util.Scanner;

public class D_Ali_BabaandPuzzles {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        // int a,b,c;
        Long a,b,c,d;
        a=sc.nextLong();
        b=sc.nextLong();
        c=sc.nextLong();
        d=sc.nextLong();
        sc.close();
        if(a+b*c==d)
            System.out.println("YES");
        else if(a+b-c==d)
            System.out.println("YES");
        else if(a-b+c==d)
            System.out.println("YES");
        else if(a-b*c==d)
            System.out.println("YES");
        else if(a*b+c==d)
            System.out.println("YES");
        else if(a*b-c==d)
            System.out.println("YES");
        else 
            System.out.println("NO");
    }
}
