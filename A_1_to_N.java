import java.util.Scanner;

public class A_1_to_N {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        sc.close();
        int i=0;
        while(i++<n){
            System.out.println(i);
        }
        
    }
}
