import java.util.Scanner;

public class C_Next_Alphabet {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        char c=sc.next().charAt(0);
        sc.close();
        System.out.println((c==122)?(char)(c-25):(char)(c+1));
    
    }
}
