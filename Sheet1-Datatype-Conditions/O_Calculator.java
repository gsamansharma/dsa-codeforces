import java.util.Scanner;

public class O_Calculator {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String exp=sc.next();
        sc.close();
        int n=exp.length();
        int operation=-1;
        for(int i=0;i<n;i++){
            if(exp.charAt(i)<48)
                operation=i;
        }
        int a=Integer.parseInt(exp.substring(0, operation));
        int b=Integer.parseInt(exp.substring(operation+1, n));
        switch(exp.charAt(operation)){
            case '+':System.out.println(a+b);
            break;
            case '-':System.out.println(a-b);
            break;
            case '*':System.out.println(a*b);
            break;
            case '/':System.out.println(a/b);
            break;
        }
        
    }
}
