import java.util.Scanner;
import java.lang.Math;
public class H_Two_Numbers{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        sc.close();
        System.out.println("floor "+a+" / "+b+" = "+(int)Math.floor(a/b));
        System.out.println("ceil "+a+" / "+b+" = "+(int)Math.ceil(1.0*a/b));
        System.out.println("round "+a+" / "+b+" = "+Math.round(1.0*a/b));
    }
}