import java.util.Scanner;

public class R_Age_in_Days {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int days=sc.nextInt();
        sc.close();
        int years,months;
        years=days/365;
        days%=365;
        months=days/30;
        days%=30;
        System.out.println(years+" years\n"+months+" months\n"+days+" days\n");
        
    }
}
