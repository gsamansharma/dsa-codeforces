import java.util.Scanner;

public class U_Float_or_int {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String input=sc.next();        
        float x=Float.parseFloat(input);
        short y=(short)x;
        sc.close();
        if(x==y){
            System.out.println("int "+y);
        }
        else{
            System.out.printf("float %d %.3f",y,x-y);
        }

    }
}
