import java.util.Scanner;

public class Q_Coordinates_of_a_Point {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double a=sc.nextDouble();
        double b=sc.nextDouble();
        sc.close();
        if(a>0&&b>0)
            System.out.print("Q1");
        else if(a<0&&b>0)
            System.out.print("Q2");
        else if(a<0&&b<0)
            System.out.print("Q3");
        else if(a>0&&b<0)
            System.out.print("Q4");
        else if(a==0&&b==0)
            System.out.print("Origem");
        else if(b==0)
            System.out.print("Eixo X");
        else if(a==0)
            System.out.print("Eixo Y");
            
    }
}
