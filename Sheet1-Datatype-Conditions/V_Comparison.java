import java.util.Scanner;

public class V_Comparison {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        char op=sc.next().charAt(0);
        int b=sc.nextInt();
        sc.close();
        if((op=='>'&&a>b)||(op=='<'&&a<b)||(op=='='&&a==b)){
            System.out.println("Right");
        }
        else{
            System.out.println("Wrong");
        }
    }
}
