import java.util.Scanner;

public class N_Char {
   public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        char c=sc.next().charAt(0);
        sc.close();
        System.out.println((c<=90)?Character.toLowerCase(c):Character.toUpperCase(c));
   }
}
