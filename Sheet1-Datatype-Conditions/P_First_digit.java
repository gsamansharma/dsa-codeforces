import java.util.Scanner;

public class P_First_digit {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        sc.close();
        n/=1000;
        System.out.println((n & 1) == 1 ? "ODD" : "EVEN");
    }
}
