import java.util.Scanner;
public class B_Basic_Data_Types{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int i=sc.nextInt();
        System.out.println(i);
        Long l=sc.nextLong();
        System.out.println(l);
        char c=sc.next().charAt(0);
        System.out.println(c);
        float f=sc.nextFloat();
        System.out.println(f);
        double d=sc.nextDouble();
        System.out.println(d);
        sc.close();
    }
}