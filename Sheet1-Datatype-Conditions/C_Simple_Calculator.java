import java.util.Scanner;

public class C_Simple_Calculator{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int m=sc.nextInt();
        int n=sc.nextInt();
        System.out.println(m+" + "+n+" = "+(m+n));
        System.out.println(m+" * "+n+" = "+((long)m*n));
        System.out.println(m+" - "+n+" = "+(m-n));
        sc.close();
    }
}