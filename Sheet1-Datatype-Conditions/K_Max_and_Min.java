import java.util.Scanner;

public class K_Max_and_Min {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        sc.close();
        int min=(a<b)?((a<c)?a:c):((b<c)?b:c);
        int max=(a>b)?((a>c)?a:c):((b>c)?b:c);
        System.out.println(min+" "+max);
    }
}