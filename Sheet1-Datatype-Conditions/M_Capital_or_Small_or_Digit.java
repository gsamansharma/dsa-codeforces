import java.util.Scanner;

public class M_Capital_or_Small_or_Digit {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        char c=sc.next().charAt(0);
        sc.close();
        System.out.println((c>=48&&c<=57)?"IS DIGIT":((c>=65&&c<=90)?"ALPHA\nIS CAPITAL":"ALPHA\nIS SMALL"));
    }
}
