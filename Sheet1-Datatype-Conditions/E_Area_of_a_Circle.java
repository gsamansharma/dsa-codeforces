import java.util.Scanner;

public class E_Area_of_a_Circle {
    public static void main(String[] args) {
        final double pi= 3.141592653;
        Scanner sc=new Scanner(System.in);
        double r=sc.nextDouble();
        sc.close();
        System.out.format("%.9f%n", pi*r*r);
    }
}
