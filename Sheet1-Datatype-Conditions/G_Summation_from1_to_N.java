
import java.util.Scanner;

public class G_Summation_from1_to_N {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        long n=sc.nextInt();
        sc.close();
        long sum=(n*++n)>>1;
        System.out.println(sum);
    }
}
