import java.util.Arrays;
import java.util.Scanner;

public class T_Sort_Numbers {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int array[]=new int[3];
        array[0]=sc.nextInt();
        array[1]=sc.nextInt();
        array[2]=sc.nextInt();
        sc.close();
        
        int sorted_array[]=array.clone();
        Arrays.sort(sorted_array);
        for(int i:sorted_array){
            System.out.println(i);
        }
        System.out.println();
        for(int i:array){
            System.out.println(i);
        }
        
    }
}
