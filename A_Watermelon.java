import java.util.Scanner;

public class A_Watermelon {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        byte weight=sc.nextByte();
        sc.close();
        System.out.println((weight%2==0&&weight>2)?"YES":"NO");
    }
}
