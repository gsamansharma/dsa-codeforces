import java.util.Scanner;

public class G_Factorial {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            int n=sc.nextInt();
            System.out.println(fact(n));
        }
    }
    public static long fact(int n){
        if(n==1||n==0)
        return 1;
        return n*fact(n-1);
    }
}
